-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : sam. 23 mai 2020 à 15:02
-- Version du serveur :  10.2.31-MariaDB

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `ocarugfu_todolist`
--

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20200416165615', '2020-05-17 20:07:29'),
('20200416201102', '2020-05-17 20:07:29'),
('20200504142429', '2020-05-17 20:07:29'),
('20200504171952', '2020-05-17 20:07:29'),
('20200517190849', '2020-05-17 20:07:29');

-- --------------------------------------------------------

--
-- Structure de la table `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_done` tinyint(1) NOT NULL,
  `author_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `task`
--

INSERT INTO `task` (`id`, `created_at`, `title`, `content`, `is_done`, `author_id`) VALUES
(1, '2020-05-17 22:07:39', 'Vérifier les specs', 'Corriger les fautes d\'orthographe et inclure les schémas UML au document', 0, 7),
(2, '2020-05-17 22:07:39', 'UML', 'Réaliser les schémas UML pour la nouvelle application avec Visual Paradigm', 1, 2),
(3, '2020-05-17 22:07:39', 'Board', 'Mettre au point un board avancement. Ajouter tous les acteurs qui interviendront sur le projet', 0, 3),
(4, '2020-05-17 22:07:39', 'Graphisme', 'Retravailler les images et cherchant d\'autres photos d’illustration sur le thème \"ToDo\"', 1, 4),
(6, '2020-05-17 22:07:39', 'Maquettes', 'Réaliser des mockups pour faire évoluer l\'intégration de l\'application', 0, 6),
(7, '2020-05-20 10:07:48', 'Réunion de travail', 'Planifier des réunions de travail régulières pour piloter l\'avancement du projet', 0, 1),
(8, '2020-05-21 16:43:10', 'Lorem ipsum', 'Powder apple pie bonbon tootsie roll fruitcake gummi bears. Icing sweet ice cream. Carrot cake dessert cotton candy jelly beans sweet roll oat cake marzipan. Jelly beans topping tart soufflé.', 0, 8);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `roles`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$13$4eyTJdgz0.NAio0SH5OSK.6ntDCsML5uWEUVj3xkNObX5cbPLAHW6', 'admin@todolist-oc.fr', '[\"ROLE_ADMIN\"]', '2020-01-15 13:52:04', '2020-05-21 12:55:42'),
(2, 'Raymond', '$2y$13$7aIPWjQQ2cHAoIMfmmzWpe42B/fHBWsMkH2RcRMuci7.mCvucEL.e', 'raymond@example.org', '[\"ROLE_USER\"]', '2019-10-27 02:15:30', '2020-05-21 12:56:49'),
(3, 'Arnaude', '$2y$13$7aIPWjQQ2cHAoIMfmmzWpe42B/fHBWsMkH2RcRMuci7.mCvucEL.e', 'arnaude@example.com', '[\"ROLE_USER\"]', '2020-03-17 07:05:27', '2020-05-21 12:56:22'),
(4, 'Lucy', '$2y$13$7aIPWjQQ2cHAoIMfmmzWpe42B/fHBWsMkH2RcRMuci7.mCvucEL.e', 'lucy@example.org', '[\"ROLE_USER\"]', '2019-07-27 07:12:12', '2020-05-21 12:57:09'),
(6, 'André', '$2y$13$7aIPWjQQ2cHAoIMfmmzWpe42B/fHBWsMkH2RcRMuci7.mCvucEL.e', 'andre@example.org', '[\"ROLE_USER\"]', '2019-06-23 12:15:50', '2020-05-21 12:57:22'),
(7, 'anonymous', '$2y$13$7aIPWjQQ2cHAoIMfmmzWpe42B/fHBWsMkH2RcRMuci7.mCvucEL.e', 'anonymous@anonym.ous', '[\"ROLE_USER\"]', '2020-05-17 22:07:58', '2020-05-17 22:07:58');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_527EDB25F675F31B` (`author_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `FK_527EDB25F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
