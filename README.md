# ToDoList

This project is a training project. It's the 8th Project of PHP / Symfony web developer cursus of OpenClassRooms.

The objective is to improve an existing ToDo application build with Symfony 3.1.6 and Bootstrap 3.3.7, to carry out a performance audit and set up functional and unit tests

The previous version of initial version (MVP) is available [here][17]. This version had some anomalies and was missing important features.

[![pipeline status](https://gitlab.com/armelle-formation/todolist/badges/develop/pipeline.svg)](https://gitlab.com/armelle-formation/todolist/-/commits/develop)
[![coverage report](https://gitlab.com/armelle-formation/todolist/badges/develop/coverage.svg)](https://gitlab.com/armelle-formation/todolist/-/commits/develop)
[![Quality Gate](public/assets/img/quality_gate.jpg)](public/assets/img/quality_gate.jpg)
---

## Requirements
* [MySQL 5.0.12][1]
* [PHP 7.4.3][2]
* [Apache 2.4.38][3]

## Build with
### Environment
* Symfony 5.0.7
* [Composer 1.9.0][4]

### Bundles
  * Twig
  * [Font-awesome 5.9.0][10]
  * [PHPunit][5]
  * [fzaninotto Faker][6] - dev only (tools for fake data)

### Dependencies
  * [Boostrap 4.4.1][7]
  * [jQuery 3.4.1][8]
  * [Isotope 3.0.6][9]
  
## Installation 
* Clone the repository
```
  git clone https://gitlab.com/armelle-formation/todolist.git
```
* Install dependencies.
```
  composer install
```
## Configuration
### Environment
* Duplicate the .env.dist file and customize it with your database identifiers
```
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name"`
```
* Rename this file in .env and upload it on your server

### Encrypted Secret Keys for DATABASE URL
1. Generate the keys used to encrypt/decrypt secrets
```
php bin/console secrets:generate-keys
```
2. Create a new secret to store the contents of `DATABASE_URL` (The secret value must be all string after `DATABASE_URL=`)
```
 php bin/console secrets:set DATABASE_URL

 Please type the secret value:
 **************
```
3. remove `DATABASE_URL` of your `.env` file.

### Database
* Create database 
```
  php bin/console doctrine:database:create
```
Import todolist.sql file in your database 

Your can also generate fake content (warning : you must be in dev environnement : remember to update your .env file)

* Get tables 
```
  php bin/console doctrine:migrations:migrate
```
* Load fixtures
```
  php bin/console doctrine:fixtures:load  
```

### Access
Whether you use the .sql file or fixtures, an admin account is already created.
If you keep using this sources, create new account and delete this one

```
"username" : "Admin",
"password" : "Password#123"
```

## Code quality and performances
### Quality & maintainability
This project got Medal A on SonarQube (HTML, CSS ans PHP PSR-2).
The PHP Coding Standards Fixer (PHP CS Fixer) tool was used to fixes code during conception.

### Unit & Functional tests
To improve test performance, in-memory SQLite isolated test database was used (which is recommended for most functional-tests). See `.env.test` file. At each test sequence, a fresh, predictable database was created for every test. In order to run tests even faster, the database is also cached. This will create backups of the initial database (with all fixtures loaded) and re-load them when required.

Unit tests require :
- [PHP Unit][5]
- [LiipTestFixturesBundle][18]
- XDebug php extension

Read [this][18] for more informations.

To run tests manually you only have to run this command: 
```
php bin/phpunit
```

### Continuous integration
Continuous integration has been implemented with Gitlab CI. 
At each push on the remote develop branch, two tests are executed: code quality and unit tests. These two tests must be passed successfully for the merge on master branch to be accepted.

## Audit
Application performances were analyzed with [BlackFire][19] tool.
Performances graphs and full code audit can be found in [Audit report][20].

## Documentation
In [documentation][11] folder you can find:
* a technical documentation about [authentication][12],
* the [changelog file][13] of project.
* [code coverage reports][14].
* [UML diagrams][15]

## Contributing
To contribute to this project, please see [How to contribute instructions][16]

## Project status
Development has slowed down but not stopped. You can contact me if you need to help or more informations

---

## Copyright and License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.

[0]: https://todolist.oc-armellebraud.fr
[1]: https://www.mysql.com/fr/
[2]: http://php.net/manual/fr/intro-whatis.php
[3]: https://www.apache.org/
[4]: https://getcomposer.org/download/
[5]: https://phpunit.de
[6]: https://github.com/fzaninotto/Faker
[7]: https://getbootstrap.com/
[8]: https://jquery.com/
[9]: https://isotope.metafizzy.co/
[10]: https://fontawesome.com/
[11]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/
[12]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/authentication/AUTHENTICATION.md
[13]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/releases_notes/CHANGELOG.md
[14]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/codecoverage/
[15]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/diagrams/
[16]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/contributing/CONTRIBUTING.md
[17]: https://github.com/saro0h/projet8-TodoList
[18]: https://github.com/liip/LiipTestFixturesBundle/blob/HEAD/doc/database.md
[19]: https://blackfire.io/
[20]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/audit/audit.pdf