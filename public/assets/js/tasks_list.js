$(document).ready(function() {
    var grid = $(".grid").isotope({
        itemSelector: ".grid-item",
        layoutMode: 'masonry',
    });

    $(".filters").on("click", ".filter", function() {
        var classe = $(this).attr("data-filter");
        grid.isotope({ filter: classe });
    });
});