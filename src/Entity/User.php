<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *  fields={"email"},
 *  message="Cet email a déjà été utilisé, merci de le modifier"
 * )
 * @UniqueEntity(
 *  fields={"username"},
 *  message="Ce nom d'utilisateur existe déjà dans notre liste d'utilisateurs."
 * )
 */
class User implements UserInterface
{
    use TimeStampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Vous devez saisir un nom d'utilisateur.")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Le nom d'utilisateur doit faire plus de {{ limit }} caractères",
     *      maxMessage = "Le nom d'utilisateur ne peut pas faire plus de {{ limit }} caractères"
     * )
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 8,
     *      max = 200,
     *      minMessage = "Votre mot de passe doit contenir au moins {{ limit }} caractères.",
     *      maxMessage = "Votre mot de passe ne peut pas contenir plus de {{ limit }} caractères"
     * )
     * @Assert\Regex(
     *     pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W)^",
     *     match = true,
     *     message = "Votre mot de passe doit contenir au moins une minuscule, une majuscule, un chiffre et un caractère spécial."
     * )
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Vous devez saisir une adresse email.")
     * @Assert\Email(message="Le format de l'adresse email n'est pas correcte.")
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Assert\Choice(callback="getAuthorizedRoles", message="Le role sélectionné ne fait pas partie des choix autorisés.")
     */
    private $roles = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="author", cascade={"remove"})
     */
    private $tasks;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        if (null !== $password) {
            $this->password = $password;
        }

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     * @codeCoverageIgnore
     */
    public function getSalt()
    {
    }

    /**
     * @see UserInterface
     * @codeCoverageIgnore
     */
    public function eraseCredentials()
    {
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setAuthor($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
            // set the owning side to null (unless already changed)
            if ($task->getAuthor() === $this) {
                $task->setAuthor(null);
            }
        }

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * UpdatedAt initialisation.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function initUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    public static function getAuthorizedRoles()
    {
        return [['ROLE_USER'], ['ROLE_ADMIN']];
    }

    /**
     * Get wether task is from anonymous user.
     *
     * @return bool
     */
    public function isAnonymous()
    {
        if ('anonymous' == $this->username) {
            return true;
        }

        return false;
    }

    /**
     * Get wether user is an admin.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return in_array('ROLE_ADMIN', $this->getRoles());
    }

    /**
     * Checks that the update date is much higher than the creation date.
     *
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context)
    {
        if (!empty($this->updatedAt) && ($this->updatedAt->getTimestamp() < $this->createdAt->getTimestamp())) {
            $context->buildViolation("La date de mise à jour doit être supérieure à la date de création de l'utilisateur")
                ->atPath('updatedDate')
                ->addViolation();
        }
    }
}
