<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Task
{
    use TimeStampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Vous devez saisir un titre.")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Le titre doit faire plus de {{ limit }} caractères",
     *      maxMessage = "Le titre ne peut pas faire plus de {{ limit }} caractères"
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Vous devez saisir une description.")
     * @Assert\Length(
     *      min = 20,
     *      max = 500,
     *      minMessage = "La description de la tâche doit faire plus de {{ limit }} caractères",
     *      maxMessage = "La description de la tâche ne peut pas faire plus de {{ limit }} caractères"
     * )
     */
    private $content;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDone = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tasks")
     * @ORM\JoinColumn(nullable=true)
     * @Assert\Valid
     */
    private $author;

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getIsDone(): ?bool
    {
        return $this->isDone;
    }

    public function setIsDone(bool $isDone): self
    {
        $this->isDone = $isDone;

        return $this;
    }

    public function getAuthor(): ?User
    {
        if (null === $this->author) {
            return $this->getFakeAnonymousUser();
        }

        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Toggle status of a task.
     *
     * @param bool $flag
     */
    public function toggle($flag)
    {
        $this->isDone = $flag;
    }

    /**
     * Get whether task is from anonymous author.
     *
     * @return bool
     */
    public function isAnonymous()
    {
        return 'anonymous' === strtolower($this->getAuthor()->getUsername());
    }

    /**
     * Créer un faux utilisateur anonyme (useful for units test only).
     *
     * @return User
     */
    private function getFakeAnonymousUser()
    {
        $user = new User();
        $user->setUsername('fakeAnonymousUser');

        return $user;
    }
}
