<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\HttpCache\HttpCache;

/**
 * @codeCoverageIgnore
 */
class CacheKernel extends HttpCache
{
}
