<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Updates old tasks without author and set anonymous author.
 */
class UpdateOldTasksCommand extends Command
{
    private $manager;
    private $userRepository;
    private $taskRepository;
    private $encoder;
    protected static $defaultName = 'todolist:oldtasks:update';

    public function __construct(
        EntityManagerInterface $manager,
        TaskRepository $taskRepository,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->manager = $manager;
        $this->userRepository = $userRepository;
        $this->taskRepository = $taskRepository;
        $this->encoder = $passwordEncoder;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Updates old tasks without author and set anonymous author')
            ->setHelp('This command updates old tasks without an author and assigns them an anonymous user. This user is automatically created if it does not already exist in the database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Update old Tasks',
            '============',
            '',
        ]);

        $application = $this->getApplication();
        $application->setAutoExit(false);

        // Get anonymous user
        $anonymous = $this->getAnonymousUser();

        $tasks = $this->taskRepository->findAll();
        $nbTasksToUpdate = 0;

        foreach ($tasks as $task) {
            if (null === $task->getAuthor() || 'fakeAnonymousUser' === $task->getAuthor()->getUsername()) {
                $task->setAuthor($anonymous);
                ++$nbTasksToUpdate;

                $output->writeln([
                    'Task: ' . $task->getTitle(),
                    'Updated',
                ]);
            }
        }
        $this->manager->flush();

        $report = 0 === $nbTasksToUpdate ? 'No task without author were found.' : $nbTasksToUpdate . ' users without author have been updated with anonymous author';

        $output->writeln($report);

        return 0;
    }

    /**
     * Get anonymous user.
     *
     * @return User
     */
    private function getAnonymousUser()
    {
        $anonymousUser = $this->userRepository->findOneBy(['username' => 'anonymous']);

        return $anonymousUser instanceof User ? $anonymousUser : $this->createAnonymousUser();
    }

    /**
     * Create anonymous user.
     *
     * @return User
     */
    private function createAnonymousUser()
    {
        $anonymousUser = new User();

        $anonymousUser->setUsername('anonymous')
                ->setEmail('anonymous@anonym.ous')
                ->setPassword($this->encoder->encodePassword($anonymousUser, 'anonymousP@ssword123'))
                ->setRoles(['ROLE_USER']);

        $this->manager->persist($anonymousUser);
        $this->manager->flush();

        return $anonymousUser;
    }
}
