<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserType extends AbstractType
{
    private $roles = [];
    private $authorization = [];

    public function __construct($hierarchyRoles, AuthorizationCheckerInterface $authorizationChecker)
    {
        $roles = [];

        array_walk_recursive($hierarchyRoles, function ($val) use (&$roles) {
            $key = ucwords(strtolower(str_replace('ROLE_', '', $val)));
            $roles[$key] = $val;
        });

        $this->roles = array_unique($roles);
        $this->authorization = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $route = $options['data_route'];

        $builder
            ->add(
                'username',
                TextType::class,
                [
                    'label' => "Nom d'utilisateur",
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'Adresse email',
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Enregistrer',
                    'attr' => ['class' => 'btn btn-info'],
                ]
            )

        ;

        // Inputs password required only for create user
        if ('user_create' === $route) {
            $builder->add(
                'password',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'invalid_message' => 'Les deux mots de passe doivent correspondre.',
                    'required' => true,
                    'first_options' => ['label' => 'Mot de passe'],
                    'second_options' => ['label' => 'Tapez le mot de passe à nouveau'],
                ]
            );
        } else {
            $builder->add(
                'password',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'invalid_message' => 'Les deux mots de passe doivent correspondre.',
                    'required' => false,
                    'first_options' => ['label' => 'Mot de passe'],
                    'second_options' => ['label' => 'Tapez le mot de passe à nouveau'],
                ]
            );
        }

        // Edit role only for admin
        if ($this->authorization->isGranted('ROLE_EDITION', $options['data'])) {
            $builder->add(
                'roles',
                ChoiceType::class,
                [
                    'choices' => $this->roles,
                    'expanded' => true,
                    'required' => true,
                    'attr' => ['class' => 'inline'],
                    'label' => 'Rôle',
                        'label_attr' => [
                            'class' => 'checkbox-inline',
                        ],
                ]
            );

            $builder->get('roles')
                ->addModelTransformer(
                    new CallbackTransformer(
                        function ($rolesAsArray) {
                            return count($rolesAsArray) ? $rolesAsArray[0] : 'ROLE_USER';
                        },
                        function ($rolesAsString) {
                            return [$rolesAsString];
                        }
                    )
                );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'data_route' => null,
            'attr' => ['autocomplete' => 'off'],
        ]);
    }
}
