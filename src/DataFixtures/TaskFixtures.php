<?php

namespace App\DataFixtures;

use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class TaskFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // For simulating a old task without author
        $task = new Task();
        $task->setTitle('Tâche 0 sans auteur');
        $task->setContent('Ceci est la tâche n°' . 0 . ' sans auteur créée via les fixtures.');
        $this->setReference('Task-noauthor', $task);
        $manager->persist($task);

        // new tasks
        for ($i = 1; $i < 6; ++$i) {
            $task = new Task();

            $task->setTitle('Tâche ' . ($i));
            $task->setContent('Ceci est la tâche n°' . $i . ' créée via les fixtures.');
            $task->setAuthor($this->getReference(UserFixtures::USER_REFERENCE . '-' . $i));

            $done = 3 == $i ? 1 : 0;
            $task->setIsDone($done);

            //Reference
            $this->setReference("Task-$i", $task);

            $manager->persist($task);
        }

        // Send data to db
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
