<?php

namespace App\DataFixtures;

use App\Entity\User;
use Behat\Transliterator\Transliterator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const ADMIN_REFERENCE = 'account-admin';
    public const USER_REFERENCE = 'account';

    public function __construct(ParameterBagInterface $params, UserPasswordEncoderInterface $encoder)
    {
        $this->params = $params;
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create($this->getLocale());

        // Admin user
        $admin = new User();
        $admin->setUsername('Admin')
                ->setEmail(strtolower('armelle@todolist-oc.fr'))
                ->setPassword($this->encoder->encodePassword($admin, 'Password#123'))
                ->setRoles(['ROLE_ADMIN'])
                ->setCreatedAt($faker->dateTimeBetween('2019-03-15 02:00:49', 'Europe/Paris'));

        $manager->persist($admin);

        //Reference
        $this->setReference(self::ADMIN_REFERENCE, $admin);

        // Users
        for ($i = 1; $i < 6; ++$i) {
            $firstname = $faker->firstName;
            $email = Transliterator::unaccent($firstname . '@' . $faker->safeEmailDomain);
            $dateCreate = $faker->dateTimeBetween('2019-03-15 02:00:49', 'Europe/Paris');
            $dateUpdate = $faker->dateTimeBetween($dateCreate, 'Europe/Paris');

            $user = new User();
            $user->setUsername($firstname)
                ->setEmail(strtolower($email))
                ->setPassword($this->encoder->encodePassword($user, '456P@ssword'))
                ->setRoles(['ROLE_USER'])
                ->setCreatedAt($dateCreate)
                ->setUpdatedAt($dateUpdate);

            $manager->persist($user);

            //Reference
            $this->setReference(self::USER_REFERENCE . "-$i", $user);
        }

        $manager->flush();
    }

    public function getLocale()
    {
        return $this->params->get('locale');
    }
}
