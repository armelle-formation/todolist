<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\DbService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserController.
 *
 * @IsGranted("ROLE_ADMIN")
 * @Route("/users", name="user_")
 */
class UserController extends AbstractController
{
    /**
     * Display list of all users.
     *
     * @Route("", name="list")
     */
    public function listAction(Request $request, UserRepository $userRepository, DbService $dbService)
    {
        $response = new Response();
        $response->setEtag(md5($dbService->getLastUpdate('user')));
        $response->headers->addCacheControlDirective('no-control');
        $response->setPublic();

        if ($response->isNotModified($request)) {
            return $response;
        }

        return $this->render(
            'user/list.html.twig',
            ['users' => $userRepository->findAllWithoutAnonymous(['username' => 'anonymous'])],
            $response
        );
    }

    /**
     * Display form to add a new user.
     *
     * @Route("/create", name="create")
     *
     * @return Response
     */
    public function createAction(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(
            UserType::class,
            $user,
            [
            'data_route' => $request->attributes->get('_route'),
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', "L'utilisateur a bien été ajouté.");

            return $this->redirectToRoute('user_list');
        }

        return $this->render('user/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Display form to edit a user.
     *
     * @Route("/{id}/edit", name="edit")
     * @IsGranted("EDIT", subject="user")
     *
     * @return Response
     */
    public function editAction(User $user, Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager)
    {
        $form = $this->createForm(
            UserType::class,
            $user,
            [
            'data_route' => $request->attributes->get('_route'),
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!empty($form['password']->getData())) {
                $password = $encoder->encodePassword($user, $form['password']->getData());
                $user->setPassword($password);
            }
            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', "L'utilisateur a bien été modifié");

            return $this->redirectToRoute('user_list');
        }

        return $this->render('user/edit.html.twig', ['form' => $form->createView(), 'user' => $user]);
    }

    /**
     * Delete a user.
     *
     * @Route("/{id}/delete", name="delete")
     * @IsGranted("DELETE", subject="user")
     *
     * @return Response
     */
    public function deleteAction(User $user, EntityManagerInterface $manager)
    {
        $manager->remove($user);
        $manager->flush();

        $this->addFlash('success', "L'utilisateur a bien été supprimé.");

        return $this->redirectToRoute('user_list');
    }
}
