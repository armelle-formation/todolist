<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use App\Service\DbService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController.
 *
 * @Route("/tasks", name="task_")
 */
class TaskController extends AbstractController
{
    /**
     * Display list of all tasks.
     *
     * @Route("", name="list")
     *
     * @return Response
     */
    public function listAction(Request $request, TaskRepository $taskRepository, DbService $dbService)
    {
        $response = new Response();
        $response->setEtag(md5($dbService->getLastUpdate('task')));
        $response->headers->addCacheControlDirective('no-control');
        $response->setPublic();

        if ($response->isNotModified($request)) {
            return $response;
        }

        return $this->render(
            'task/list.html.twig',
            ['tasks' => $taskRepository->findAllOrderByMostRecent()],
            $response
        );
    }

    /**
     * Display form to add a new task.
     *
     * @Route("/create", name="create")
     *
     * @return Response
     */
    public function createAction(Request $request, EntityManagerInterface $manager)
    {
        $task = new Task();
        $task->setAuthor($this->getUser());

        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($task);
            $manager->flush();

            $this->addFlash('success', 'La tâche a été bien ajoutée.');

            return $this->redirectToRoute('task_list');
        }

        return $this->render(
            'task/create.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Display form to edit a task.
     *
     * @Route("/{id}/edit", name="edit")
     * @IsGranted("EDIT", subject="task")
     *
     * @return Response
     */
    public function editAction(Task $task, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($task);
            $manager->flush();

            $this->addFlash('success', 'La tâche a bien été modifiée.');

            return $this->redirectToRoute('task_list');
        }

        return $this->render('task/edit.html.twig', [
            'form' => $form->createView(),
            'task' => $task,
        ]);
    }

    /**
     * Change status of a task (toDo/Done).
     *
     * @Route("/{id}/toggle", name="toggle")
     * @IsGranted("EDIT", subject="task")
     */
    public function toggleTaskAction(Task $task, EntityManagerInterface $manager)
    {
        $task->setIsDone(!$task->getisDone());
        $manager->persist($task);
        $manager->flush();

        $this->addFlash('success', sprintf('La tâche %s a bien été marquée comme faite.', $task->getTitle()));

        return $this->redirectToRoute('task_list');
    }

    /**
     * Delete a task.
     *
     * @Route("/{id}/delete", name="delete")
     * @IsGranted("DELETE", subject="task")
     */
    public function deleteTaskAction(Task $task, EntityManagerInterface $manager)
    {
        $manager->remove($task);
        $manager->flush();

        $this->addFlash('success', 'La tâche a bien été supprimée.');

        return $this->redirectToRoute('task_list');
    }
}
