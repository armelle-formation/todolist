<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class TaskVoter extends Voter
{
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function supports($attribute, $task)
    {
        return in_array($attribute, ['DELETE', 'EDIT'])
            && $task instanceof \App\Entity\Task;
    }

    public function voteOnAttribute($attribute, $task, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface || $user->isAnonymous()) {
            return false;
        }

        switch ($attribute) {
            case 'DELETE':
            case 'EDIT':
                if ($user === $task->getAuthor() || $this->authorizedUserIsAdmin()) {
                    return true;
                }
            break;
        }

        return false;
    }

    private function authorizedUserIsAdmin()
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }
}
