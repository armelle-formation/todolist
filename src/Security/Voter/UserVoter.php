<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserVoter extends Voter
{
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, ['DELETE', 'EDIT', 'CREATE', 'ROLE_EDITION'])
            && $subject instanceof \App\Entity\User;
    }

    protected function voteOnAttribute($attribute, $user, TokenInterface $token)
    {
        $connectedUser = $token->getUser();
        if (!$connectedUser instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case 'CREATE':
                if ($this->authorizedUserIsAdmin()) {
                    return true;
                }
            break;
            case 'EDIT':
                if ($user === $connectedUser || $this->authorizedUserIsAdmin()) {
                    return true;
                }

                return false;
            break;
            case 'ROLE_EDITION':
            case 'DELETE':
                if ($user != $connectedUser && $this->authorizedUserIsAdmin()) {
                    return true;
                }
            break;
        }

        return false;
    }

    private function authorizedUserIsAdmin()
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }
}
