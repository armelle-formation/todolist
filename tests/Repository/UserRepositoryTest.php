<?php

namespace App\Tests\Repository;

use App\DataFixtures\UserFixtures;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserRepositoryTest extends KernelTestCase {

    use FixturesTrait;
    
    protected $fixtures;
    protected $repository;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->fixtures = $this->loadFixtures([UserFixtures::class])->getReferenceRepository();
        $this->repository = self::$container->get(UserRepository::class);
    }

    public function testCount(){

        $users = $this->repository->count([]);
        $this->assertEquals(6, $users);
    }

    public function testSearchUserByEmail()
    {
        $user = $this->repository->findOneByEmail($this->fixtures->getReference('account-admin')->getEmail());

        $this->assertSame('Admin', $user->getUsername());
        $this->assertSame('armelle@todolist-oc.fr', $user->getEmail());
        $this->assertSame('ROLE_ADMIN', $user->getRoles()[0]);

    }

    public function testFindAllWithoutAnonymous()
    {
        $allUsers = $this->repository->count([]);
        $users = $this->repository->findAllWithoutAnonymous(['username' => 'anonymous']);
        
        $this->assertEquals($allUsers, count($users));
    }   

}