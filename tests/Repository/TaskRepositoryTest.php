<?php

namespace App\Tests\Repository;

use App\Entity\Task;
use App\Entity\User;
use App\DataFixtures\TaskFixtures;
use App\Repository\TaskRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TaskRepositoryTest extends KernelTestCase {

    use FixturesTrait;
    
    protected $repository;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->loadFixtures([TaskFixtures::class]);
        $this->repository = self::$container->get(TaskRepository::class);
    }
    
    public function testCount(){

        $tasks =  $this->repository->count([]);
        $this->assertEquals(6, $tasks);
    }

    public function testSearchByTitle()
    {
        $task = $this->repository->findOneBy(['title' => 'Tâche 1']);

        $this->assertNotNull($task);
        $this->assertSame('Tâche 1', $task->getTitle());
        $this->assertSame('Ceci est la tâche n°1 créée via les fixtures.', $task->getContent());
        $this->assertInstanceOf(User::class, $task->getAuthor());
    }

    public function testSearchByUser()
    {
        $task =  $this->repository->findOneBy(['author' => 2]);

        $this->assertNotNull($task);
        $this->assertInstanceOf(Task::class, $task);
    }
}