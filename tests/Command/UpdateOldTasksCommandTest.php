<?php

namespace Tests\AppBundle\Command;

use App\DataFixtures\TaskFixtures;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use App\Command\UpdateOldTasksCommand;
use Symfony\Component\Console\Application;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UpdateOldTasksCommandTest extends KernelTestCase
{
    use FixturesTrait;
    
    public function testExecute()
    {
        self::bootKernel();

        $this->loadFixtures([TaskFixtures::class]);

        $application = new Application();
        $application->add(new UpdateOldTasksCommand(
            self::$container->get('doctrine')->getManager(),
            self::$container->get(TaskRepository::class),
            self::$container->get(UserRepository::class),
            self::$container->get(UserPasswordEncoderInterface::class)
        ));

        $command = $application->find('todolist:oldtasks:update');
        $commandTester = new CommandTester($command);

        // Task without author
        $commandTester->execute(array(
            'command'  => $command->getName()
        ));
        $output = $commandTester->getDisplay();
        $this->assertContains('1 users without author have been updated with anonymous author', $output);

        // Alls tasks have an author
        $commandTester->execute(array(
            'command'  => $command->getName()
        ));

        $output = $commandTester->getDisplay();
        $this->assertContains('No task without author were found', $output);
    }
}