<?php

namespace App\Tests\Controller;

use App\DataFixtures\TaskFixtures;
use App\DataFixtures\UserFixtures;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Symfony\Component\BrowserKit\Cookie;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @codeCoverageIgnore
 */
class AbstractTestController extends WebTestCase
{
    use FixturesTrait;

    protected $client;
    protected $fixtures;
    protected $entityManager;
    protected $userRepository;
    protected $taskRepository;

    protected function setUp(): void
    {
        
        $this->client = static::createClient();
        $this->fixtures = $this->loadFixtures([UserFixtures::class, TaskFixtures::class])->getReferenceRepository();
        $this->entityManager = $this->client->getContainer()->get('doctrine')->getManager();
        $this->userRepository = self::$container->get(UserRepository::class);
        $this->taskRepository = self::$container->get(TaskRepository::class);
    }

    protected function logIn($user)
    {
        $session = static::$container->get('session');

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    protected function createUserClient()
    {
        // Symfony faster trick for reproduce login process
        $user = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());
        $this->logIn($user);

        // Disconnect link presence test
        $crawler = $this->client->request('GET', '/');
        
        // Assertions
        $this->assertSame(1, $crawler->filter('a:contains("Se déconnecter")')->count());

        return $crawler;
    }

    protected function createAdminClient()
    {
        // Symfony faster trick for reproduce login process
        $user = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-admin')->getEmail());
        $this->logIn($user);

        // Disconnect link presence test
        $crawler = $this->client->request('GET', '/');

        // Assertions
        $this->assertSame(1, $crawler->filter('a:contains("Se déconnecter")')->count());

        return $crawler;
    }

}