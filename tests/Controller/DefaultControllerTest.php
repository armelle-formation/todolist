<?php

namespace App\Tests\Controller;

use Symfony\Component\HttpFoundation\Response;
use App\Tests\Controller\AbstractTestController;

class DefaultControllerTest extends AbstractTestController
{

    /**
     * Home page display
     *
     * @return void
     */
    public function testHomepageIsUpForUser()
    {
        $this->createUserClient();

        $crawler = $this->client->request('GET', '/');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSame(1, $crawler->filter('html:contains("Bienvenue sur Todo List")')->count());

        $buttons = $crawler->filter('a.btn.btn-outline-secondary')->each(function ($node) {
            return $node->text(null, true);
        });

        $this->assertContains('Créer une tâche', $buttons);
        $this->assertContains('Consulter la liste des tâches', $buttons);
    }

    public function testHomepageIsUpForAdmin()
    {
        $this->createAdminClient();

        $crawler = $this->client->request('GET', '/');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSame(1, $crawler->filter('html:contains("Bienvenue sur Todo List")')->count());

        $buttons = $crawler->filter('a.btn.btn-outline-secondary')->each(function ($node) {
            return $node->text(null, true);
        });

        $this->assertContains('Créer une tâche', $buttons);
        $this->assertContains('Consulter la liste des tâches', $buttons);
        $this->assertContains('Créer un utilisateur', $buttons);
        $this->assertContains('Gérer les utilisateurs', $buttons);
    }

    // TODO click sur les liens 
}