<?php

namespace App\Tests\Controller;

use Symfony\Component\HttpFoundation\Response;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use App\Tests\Controller\AbstractTestController;

class SecurityControllerTest extends AbstractTestController {

    use FixturesTrait;

    public function testLogin(){

        $crawler = $this->client->request('GET', '/login');

        // Page display
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSelectorNotExists('.alert.alert-danger');

        // Form display
        $this->assertSame(1, $crawler->filter('form')->count());
        $this->assertSame(1, $crawler->filter('input[name="_username"]')->count());
        $this->assertSame(1, $crawler->filter('input[name="_password"]')->count());
    }

    public function testLoginWithBadUsername(){

        $crawler = $this->client->request('GET', '/login');

        // Test if login field exists
        $form = $crawler->selectButton('Se connecter')->form();
        $form['_username'] = 'user1';
        $form['_password'] = 'fakepassword';
        $this->client->submit($form); 

        // Form redirect
        $this->client->followRedirect();

        // Assertions
        $this->assertSelectorExists('.alert.alert-danger');
        $this->assertSelectorTextSame('div.alert', "Utilisateur inconnu");
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }

    public function testLoginWithBadCredentials(){

        $crawler = $this->client->request('GET', '/login');

        // Test if login field exists
        $form = $crawler->selectButton('Se connecter')->form();
        $form['_username'] = 'Admin';
        $form['_password'] = 'fakepassword';
        $this->client->submit($form); 

        // Form redirect
        $this->client->followRedirect();

        // Assertions
        $this->assertSelectorExists('.alert.alert-danger');
        $this->assertSelectorTextSame('div.alert', "Identifiants invalides.");
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }


    public function testSuccessfullUserLogin()
    {
        $crawler = $this->createUserClient();

        $this->assertResponseIsSuccessful();
        $this->assertNotContains('.alert.alert-danger', $crawler);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSame(1, $crawler->filter('html:contains("Bienvenue sur Todo List")')->count());
    }

    public function testSuccessfullAdminLogin()
    {
        $crawler = $this->createAdminClient();

        $this->assertResponseIsSuccessful();
        $this->assertNotContains('.alert.alert-danger', $crawler);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSame(1, $crawler->filter('html:contains("Bienvenue sur Todo List")')->count());
    }

    public function testLogoutUsingLink()
    {
        // With a logged account
        $crawler = $this->createUserClient();
   
        // Log out using link
        $link = $crawler
            ->selectLink("Se déconnecter")
            ->link();

        // Check URI of link
        $this->assertContains('logout', $link->getUri());
        
        // Clink on link
        $this->client->click($link);
        
        // Check redirection
        $this->assertTrue($this->client->getResponse()->isRedirect('http://localhost/login'));

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        
        // Check if user is disconnect
        $buttons = $crawler->filter('button')->extract(['_text']);
        $this->assertTrue(in_array("Se connecter", $buttons));
        
    }

    public function testLogoutUsingRoute()
    {
        // With a logged account
        $this->createUserClient();
   
        // Log out using route
        $this->client->request('GET', '/logout');
        
        // Check redirection
        $this->assertTrue($this->client->getResponse()->isRedirect('http://localhost/login'));
        
        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        
        // Check if user is disconnect
        $buttons = $crawler->filter('button')->extract(['_text']);
        $this->assertTrue(in_array("Se connecter", $buttons));
    }


}
