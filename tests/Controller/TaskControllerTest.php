<?php

namespace App\Tests;

use Symfony\Component\HttpFoundation\Response;
use App\Tests\Controller\AbstractTestController;

class TaskControllerTest extends AbstractTestController
{
    /**
     * Users page is authorized for connected users only
     * @return void
     */
    public function testListActionWithoutLogin()
    {
        $crawler = $this->client->request('GET', '/tasks');
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertTrue($this->client->getResponse()->isRedirect('http://localhost/login'));
    }

    /**
     * Is tasks page redirected on login page if user is not logged in ?
     * @return void
     */
    public function testListActionWithLogin()
    {
        $this->createUserClient();
        $crawler = $this->client->request('GET', '/tasks');
        
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Check tasks in database
        $tasks = $this->taskRepository->count([]);
        $this->assertEquals(6, $tasks);

        // Check if button exists
        $this->assertContains('Créer une tâche', $crawler->filter('a.btn.btn-info')->text());

        // Check tasks display
        $crawler = $crawler->filter('h5.card-title');
        $titles = $crawler->each(function ($node, $i) {
            return $node->text(null, true);
        });
        
        $this->assertTrue(in_array('Tâche 1', $titles));
        $this->assertTrue(in_array('Tâche 2', $titles));
        $this->assertTrue(in_array('Tâche 3', $titles));
    }

    /**
     * Check if tasks of user connected have Call to action
     * @return void
     */
    public function testListActionWithCTA()
    {
        $this->createUserClient();

        // Get an user
        $user = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());

        // Get all tasks of user
        $allUserOwnedTasks = $this->taskRepository->findBy(['author' => $user]);

        // Get to tasks page
        $crawler = $this->client->request('GET', '/tasks');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Get all action of toogle forms
        $formsToggle = $crawler->filter('button.btn-toggle')->each(function ($node) {
            return $this->getRelativeFormAction($node->form()->getUri());
        });
        // Get all action of edit forms
        $formsEdit = $crawler->filter('button.btn-edit')->each(function ($node) {
            return $this->getRelativeFormAction($node->form()->getUri());
        });
        // Get all action of delete forms
        $formsDelete = $crawler->filter('button.btn-delete')->each(function ($node) {
            return $this->getRelativeFormAction($node->form()->getUri());
        });
        
        // Check if user'stasks has all their CTA
        foreach($allUserOwnedTasks as $task){
            $this->assertContains('/tasks/' . $task->getId() . '/toggle', $formsToggle);
            $this->assertContains('/tasks/' . $task->getId() . '/edit', $formsEdit);
            $this->assertContains('/tasks/' . $task->getId() . '/delete', $formsDelete);
        }
    }

    /**
     * Test response if page is not modified
     * @return void
     */
    public function testListActionWithCache()
    {
        $this->createUserClient();
        $crawler = $this->client->request('GET', '/tasks');
        $response = $this->createMock(Response::class);
        $response->method('isNotModified')->willReturn(true);

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    // Create task with form
    public function testCreateAction()
    {
        $this->createUserClient();

        // Go to task creation page
        $crawler = $this->client->request('GET', '/tasks/create');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Test if form exists
        $this->assertSame(1, $crawler->filter('input[name="task[title]"]')->count());
        $this->assertSame(1, $crawler->filter('textarea[name="task[content]"]')->count());

        // Fill the task form
        $form = $crawler->selectButton('Enregistrer')->form();
        $form['task[title]'] = 'Tache de test';
        $form['task[content]'] = 'Ceci est une tâche ajoutée via un test fonctionnel';

        // Submit form
        $this->client->submit($form);
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());

        // Save new task in DB and check if data is as expected
        $userAttempted = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());
        $task = $this->taskRepository->findOneBy(['title' => 'Tache de test']);
        $this->assertNotNull($task);
        $this->assertSame('Ceci est une tâche ajoutée via un test fonctionnel', $task->getContent());
        $this->assertSame($userAttempted, $task->getAuthor());
        
        // Check redirect
        $crawler = $this->client->followRedirect();
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Check message display
        $this->assertSelectorTextSame('p.message', 'Superbe ! La tâche a été bien ajoutée.');

        // Check if the new task is in the list
        $crawler = $crawler->filter('h5.card-title');
        $titles = $crawler->each(function ($node) {
            return $node->text(null, true);
        });
        $this->assertContains($task->getTitle(), $titles);
    }

    /**
     * Test redirection of back button on add create task page
     * @return void
     */
    public function testListWithButtonBackTo()
    {
        $this->createUserClient();
        
        // Go to task creation page
        $crawler = $this->client->request('GET', '/tasks/create');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Click on back to task list button
        $link = $crawler->selectLink('Retour à la liste des tâches')->link();
        $crawler = $this->client->click($link);
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Check tasks display
        $crawler = $crawler->filter('h5.card-title');
        $titles = $crawler->each(function ($node, $i) {
            return $node->text(null, true);
        });
        
        $this->assertTrue(in_array('Tâche 1', $titles));
        $this->assertTrue(in_array('Tâche 2', $titles));
        $this->assertTrue(in_array('Tâche 3', $titles));
    }
    
    // Editing a task by his author
    public function testEditActionByHisAuthor()
    {
        $this->createUserClient();

        // Get an user
        $user = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());

        // Get task bounded to this user
        $userOwnedTask = $this->taskRepository->findOneBy(['author' => $user]);
        $this->assertNotNull($userOwnedTask);


        $crawler = $this->client->request('GET', '/tasks/' . $userOwnedTask->getId() . '/edit');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Test if creation page field exists
        $this->assertSame(1, $crawler->filter('input[name="task[title]"]')->count());
        $this->assertSame(1, $crawler->filter('textarea[name="task[content]"]')->count());

        // Fill the form
        $form = $crawler->selectButton('Enregistrer')->form();
        $form['task[title]'] = 'Tâche modifiée';
        $form['task[content]'] = 'Cette tâche a été modifiée via un test fonctionnel';

        $this->client->submit($form);
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());

        // Save new task in DB and check if data is as expected
        $task = $this->taskRepository->findOneBy(['title' => 'Tâche modifiée']);
        $this->assertNotNull($task);
        $this->assertSame('Cette tâche a été modifiée via un test fonctionnel', $task->getContent());
        $this->assertSame($user->getUsername(), $task->getAuthor()->getUsername());

        // Check redirect
        $crawler = $this->client->followRedirect();
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Check message display and class of div
        $this->assertSelectorTextSame('p.message', 'Superbe ! La tâche a bien été modifiée.');

        // Check if the edit task is in the list
        $crawler = $crawler->filter('h5.card-title');
        $titles = $crawler->each(function ($node) {
            return $node->text(null, true);
        });
        $this->assertContains($task->getTitle(), $titles);
    }

    // Editing a task by his author
    public function testEditActionByOtherUser()
    {
        $this->createUserClient();

        // Get an user
        $user = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());

        // get all tasks of this user
        $allUserOwnedTasks = $this->taskRepository->findBy(['author' => $user]);

        // Get task not bounded to this user
        for($i = 1; $i < 6; $i++) {
            if (!in_array($i,array_column($allUserOwnedTasks, 'id'))) {
                $id = $i;
                break;
            }
        }
        $nonUserTask = $this->taskRepository->findOneBy(['id' => $id]);
        $this->assertNotNull($nonUserTask);

        $crawler = $this->client->request('GET', '/tasks/' . $nonUserTask->getId() . '/edit');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);

        // Check if task is still in DB
        $task = $this->taskRepository->findOneBy(['id' => $nonUserTask->getId()]);
        $this->assertNotNull($task);

        // Check if test if trustworthy
        $this->assertNotSame($user->getUsername(), $task->getAuthor()->getUsername());

    }

    // Check if create task form is not accessible when user is not connected
    public function testEditActionWhenNotConnected()
    {
        // Go to edit form of an user being not authenticated
        $this->client->request('GET', '/tasks/1/edit');
        $this->assertTrue($this->client->getResponse()->isRedirect('http://localhost/login'));
    }

    /**
     * Checking the status change of a task
     * @return void
     */
    public function testToggleTaskActionByAdmin()
    {
        $this->createAdminClient();

        // Check if task 1 is in DB
        $task = $this->taskRepository->findOneBy(['id' => 1]);
        $this->assertNotNull($task);
        $this->assertFalse($task->getIsDone());

        // Go to task list
        $crawler = $this->client->request('GET', '/tasks');

        // Assert that Tâche 1 is in task list
        $titles = $crawler->filter('h5.card-title')->each(function ($node) {
            return $node->text(null, true);
        });
        $this->assertContains($task->getTitle(), $titles);

        // Toggle as status
        $getIsDone = $task->getIsDone();
        $this->client->request('GET', '/tasks/' . $task->getId() . '/toggle');
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());

        // Check redirect
        $crawler = $this->client->followRedirect();
        $this->assertRouteSame('task_list');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Check flash message display
        $this->assertSelectorTextSame('p.message', 'Superbe ! La tâche ' . $task->getTitle() . ' a bien été marquée comme faite.');

        // Check if task is already in task list
        $titles = $crawler->filter('h5.card-title')->each(function ($node) {
            return $node->text(null, true);
        });
        $this->assertContains($task->getTitle(), $titles);

        // Assert task id = 1 is now done in DB
        $task = $this->taskRepository->findOneBy(['id' => 1]);
        $this->assertNotSame($getIsDone, $task->getIsDone());
    }

    public function testToggleTaskActionByAuthor(){

        $this->createUserClient();

        // Get an user
        $user = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());

        // Get task bounded to this user
        $userOwnedTask = $this->taskRepository->findOneBy(['author' => $user]);
        $this->assertNotNull($userOwnedTask);

        // Toggle status
        $getIsDone = $userOwnedTask->getIsDone();
        $this->client->request('GET', '/tasks/' . $userOwnedTask->getId() . '/toggle');
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());

        // Check redirect
        $crawler = $this->client->followRedirect();
        $this->assertRouteSame('task_list');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Check flash message display
        $this->assertSelectorTextSame('p.message', 'Superbe ! La tâche ' . $userOwnedTask->getTitle() . ' a bien été marquée comme faite.');

        // Check if task is already in task list
        $titles = $crawler->filter('h5.card-title')->each(function ($node) {
            return $node->text(null, true);
        });
        $this->assertContains($userOwnedTask->getTitle(), $titles);

        // Assert task id = 1 is now done in DB
        $task = $this->taskRepository->findOneBy(['id' => $userOwnedTask->getid()]);
        $this->assertNotSame($getIsDone, $task->getIsDone());
    }

    public function testToggleTaskActionByNonAuthor()
    {
        $this->createUserClient();

        // Get an user
        $user = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());
        
        // get all tasks of this user
        $allUserOwnedTasks = $this->taskRepository->findBy(['author' => $user]);

        // Get task not bounded to this user
        for($i = 1; $i < 6; $i++) {
            if (!in_array($i,array_column($allUserOwnedTasks, 'id'))) {
                $id = $i;
                break;
            }
        }
        $nonUserTask = $this->taskRepository->findOneBy(['id' => $id]);
        $this->assertNotNull($nonUserTask);

        // Toggle status
        $getIsDone = $nonUserTask->getIsDone();
        $this->client->request('GET', '/tasks/' . $nonUserTask->getId() . '/toggle');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);

        // Check if task is still in DB
        $task = $this->taskRepository->findOneBy(['id' => $nonUserTask->getId()]);
        $this->assertSame($getIsDone, $task->getIsDone());
    }

    /**
     * Checks that an user can delete his tasks
     * @return void
     */ 
    public function testDeleteTaskActionByAuthor()
    {
        $this->createUserClient();

        // Get an user
        $user = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());

        // Get task bounded to this user
        $userOwnedTask = $this->taskRepository->findOneBy(['author' => $user]);
        $this->assertNotNull($userOwnedTask);

        //test delete
        $this->client->request('GET', '/tasks/' . $userOwnedTask->getId() . '/delete');
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());

        // Check redirect
        $crawler = $this->client->followRedirect();
        $this->assertRouteSame('task_list');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Check flash message display
        $this->assertSelectorTextSame('p.message', 'Superbe ! La tâche a bien été supprimée.');

        // Check if task is not already in task list
        $titles = $crawler->filter('h5.card-title')->each(function ($node) {
            return $node->text(null, true);
        });
        $this->assertNotContains($userOwnedTask->getTitle(), $titles);

        // Check if task 1 is in DB
        $taskDeleted = $this->taskRepository->findOneBy(['id' => $userOwnedTask->getID()]);
        $this->assertNull($taskDeleted);

    }
    
    /**
     * Checks admin has right to delete all tasks
     * @return void
     */ 
    public function testDeleteTaskActionByAdmin()
    {
        $this->createAdminClient();

        // Get an user
        $user = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-3')->getEmail());
        
        // get one task of this user
        $taskNotOwnedByAdmin = $this->taskRepository->findOneBy(['author' => $user]);
        $this->assertNotNull($taskNotOwnedByAdmin);

        $this->client->request('GET', '/tasks/'. $taskNotOwnedByAdmin->getId() .'/delete');
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());

        // Check redirect
        $crawler = $this->client->followRedirect();
        $this->assertRouteSame('task_list');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Check flash message display
        $this->assertSelectorTextSame('p.message', 'Superbe ! La tâche a bien été supprimée.');

        // Check if task is not already in task list
        $titles = $crawler->filter('h5.card-title')->each(function ($node) {
            return $node->text(null, true);
        });
        $this->assertNotContains($taskNotOwnedByAdmin->getTitle(), $titles);

        // Check if task is still in DB
        $task = $this->taskRepository->findOneBy(['id' => $taskNotOwnedByAdmin->getId()]);
        $this->assertNull($task);
    }

    /**
     * Checks that an user is only allowed to delete their own tasks
     * @return void
     */ 
    public function testDeleteTaskActionByOtherUser()
    {
        $this->createUserClient();

        // Get an user
        $user = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());
        
        // get all tasks of this user
        $allUserOwnedTasks = $this->taskRepository->findBy(['author' => $user]);

        // Get task not bounded to this user
        for($i = 1; $i < 6; $i++) {
            if (!in_array($i,array_column($allUserOwnedTasks, 'id'))) {
                $id = $i;
                break;
            }
        }
        $nonUserTask = $this->taskRepository->findOneBy(['id' => $id]);
        $this->assertNotNull($nonUserTask);

        //test delete
        $this->client->request('GET', '/tasks/'. $nonUserTask->getId() .'/delete');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);

        // Check if task is still in DB
        $task = $this->taskRepository->findOneBy(['id' => $nonUserTask->getId()]);
        $this->assertNotNull($task);
    }

    // Transform absolute url in a relative url
    private function getRelativeFormAction($url){
        $urlComponents = parse_url($url);
        $action = "{$urlComponents['scheme']}://{$urlComponents['host']}";
        return str_replace($action, '', $url);
    }

}