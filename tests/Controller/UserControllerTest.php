<?php

namespace App\Tests\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Tests\Controller\AbstractTestController;

class UserControllerTest extends AbstractTestController
{
    /**
     * Users page is authorized for all
     * TODO : change http status to 403 + return to login
     * @return void
     */
    public function testListActionWithoutLogin()
    {
        $this->client->request('GET', '/users');
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);

        $crawler = $this->client->followRedirect();

        //Check if redirection to login page
        $this->assertEquals(1, $crawler->filter('html:contains("Se connecter")')->count());
    }

    public function testListActionAccessWithUserLogin()
    {
        $this->createUserClient();

        $this->client->request('GET', '/users');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    public function testListActionAccessWithAdminLogin()
    {
        $this->createAdminClient();

        $this->client->request('GET', '/users');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testListActionDisplayPage()
    {
        $this->createAdminClient();

        // Go to users page
        $crawler = $this->client->request('GET', '/users');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('user_list');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

         // Check tasks in database
         $users = $this->userRepository->count([]);
         $this->assertEquals(6, $users);

        // Check users display
        $list = $crawler->filter('th')->extract(['_text']);
        $this->assertContains('1', $list);
        $this->assertContains('6', $list);

        // Get all link in array
        $links = $crawler->filter('a.btn.btn-success')->each(function($node) {
            $href  = $node->attr('href');
            $text  = $node->text();
        
            return compact('href', 'text');
        });

        // Check if every user has a edit link
        $this->assertTrue(in_array('/users/6/edit', array_column($links, 'href')));
        $this->assertEquals(6, count($links));
    }

    /**
     * Test response if page is not modified
     * @return void
     */
    public function testListActionWithCache()
    {
        $this->createAdminClient();
        $crawler = $this->client->request('GET', '/users');

        $response = $this->createMock(Response::class);
        $response->method('isNotModified')->with(Request::class)->willReturn(true);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testCreateActionUser()
    {
        $this->createUserClient();

        $this->client->request('GET', '/users/create');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    public function testCreateActionAdmin()
    {
        $this->createAdminClient();

        // Go to user creation page
        $crawler = $this->client->request('GET', '/users/create');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Test if form exists
        $this->assertSame(1, $crawler->filter('input[name="user[username]"]')->count());
        $this->assertSame(1, $crawler->filter('input[name="user[email]"]')->count());

        // Add user with form
        $form = $crawler->selectButton('Enregistrer')->form();

         // Fill the task form
        $form['user[username]'] = 'John';
        $form['user[password][first]'] = 'Password#123';
        $form['user[password][second]'] = 'Password#123';
        $form['user[email]'] = 'john@domain.fr';
        $form['user[roles]'] = 'ROLE_USER';

        $this->client->submit($form);
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());

        // Save new user in DB and check if data is as expected
        $user = $this->userRepository->findOneBy(['username' => 'John']);
        $this->assertNotNull($user);
        $this->assertSame('John', $user->getUsername());
        $this->assertSame('john@domain.fr', $user->getEmail());

        // Check redirect
        $crawler = $this->client->followRedirect();
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Assert flash message is displayed
        $this->assertSelectorTextSame('p.message', "Superbe ! L'utilisateur a bien été ajouté.");

        // Assert that the new user is in the list
        $cells = $crawler->filter('td')->extract(['_text']);
        $this->assertContains('John', $cells);

    }

    public function testEditActionAdmin()
    {
        $this->createAdminClient();

        // Get an user
        $userToEdit = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());
        $this->assertNotNull($userToEdit);

        // Go to user edition page
        $crawler = $this->client->request('GET', '/users/' . $userToEdit->getId() . '/edit');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Test if form exists
        $this->assertSame(1, $crawler->filter('input[name="user[username]"]')->count());
        $this->assertSame(1, $crawler->filter('input[name="user[email]"]')->count());

        // Edit user with form
        $oldPassword = $userToEdit->getPassword();
        $form = $crawler->selectButton('Enregistrer')->form();

        $form['user[username]'] = 'Jane';
        $form['user[email]'] = 'jane@domain.fr';

        $this->client->submit($form);
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());

        // Save new task in DB and check if data is as expected
        $user = $this->userRepository->findOneBy(['username' => 'Jane']);
        $this->assertNotNull($user);
        $this->assertSame('jane@domain.fr', $user->getEmail());
        $this->assertSame($oldPassword, $user->getPassword());

        // Check redirect
        $crawler = $this->client->followRedirect();
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Check message display and class of div
        $this->assertSelectorTextSame('p.message', "Superbe ! L'utilisateur a bien été modifié");

        // Assert that the edited user is in the list
        $cells = $crawler->filter('td')->extract(['_text']);
        $this->assertContains('Jane', $cells);
    }

    public function testEditHimselfAction()
    {
        $this->createAdminClient();

        // Get current admin user for edit
        $admin = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-admin')->getEmail());
        $this->assertNotNull($admin);

        // Go to user edition page
        $crawler = $this->client->request('GET', '/users/' . $admin->getId() . '/edit');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Test if form exists
        $this->assertSame(1, $crawler->filter('input[name="user[username]"]')->count());
        $this->assertSame(1, $crawler->filter('input[name="user[email]"]')->count());

        //Check if radio button exists
        $this->assertSame(0, $crawler->filter('input[name="user[roles]"]')->count());

        // Edit user with form
        $form = $crawler->selectButton('Enregistrer')->form();

        //Change username
        $form['user[username]'] = 'Armelle Braud';
        $form['user[password][first]'] = 'Password#123';
        $form['user[password][second]'] = 'Password#123';
        $form['user[email]'] = 'abraud@domain.fr';

        // Submit Form
        $this->client->submit($form);
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());

        // Save new task in DB and check if data is as expected
        $user = $this->userRepository->findOneBy(['username' => 'Armelle Braud']);
        $this->assertNotNull($user);
        $this->assertSame('abraud@domain.fr', $user->getEmail());

        // Check redirect
        $crawler = $this->client->followRedirect();
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Check message display and class of div
        $this->assertSelectorTextSame('p.message', "Superbe ! L'utilisateur a bien été modifié");

        // Assert that the edited user is in the list
        $cells = $crawler->filter('td')->extract(['_text']);
        $this->assertContains('Armelle Braud', $cells);

    }

    public function testEditActionUser()
    {
        $this->createUserClient();

        // Get an user
        $userToEdit = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());
        $this->assertNotNull($userToEdit);

        // Go to user edition page
        $this->client->request('GET', '/users/' . $userToEdit->getId() . '/edit');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    public function testDeleteActionUser()
    {
        $this->createUserClient();

        // Get an user
        $userToEdit = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());
        $this->assertNotNull($userToEdit);

        // Go to user edition page
        $this->client->request('GET', '/users/' . $userToEdit->getId() . '/delete');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    public function testDeleteActionAdmin()
    {
        $this->createAdminClient();

        // Get an user
        $userToDelete = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());
        $this->assertNotNull($userToDelete);

        // Go to user delete page
        $this->client->request('GET', '/users/' . $userToDelete->getId() . '/delete');
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());

        // Save new task in DB and check if data is as expected
        $userDeleted = $this->userRepository->findOneBy(['id' => $userToDelete->getId()]);
        $this->assertNull($userDeleted);

        // Check redirect
        $crawler = $this->client->followRedirect();
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Check message display and class of div
        $this->assertSelectorTextSame('p.message', "Superbe ! L'utilisateur a bien été supprimé.");

        // Assert that the edited user is in the list
        $cells = $crawler->filter('td')->extract(['_text']);
        $this->assertNotContains($userToDelete->getUsername(), $cells);
    }

    public function testDeleteCurrentUserAction()
    {
        $this->createAdminClient();

        // Get current admin user for edit
        $adminToDelete = $this->userRepository->findOneByEmail($this->fixtures->getReference('account-admin')->getEmail());
        $this->assertNotNull($adminToDelete);

        // Go to user deletion page
        $crawler = $this->client->request('GET', '/users/' . $adminToDelete->getId() . '/delete');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);

        // Check if user is still in DB
        $adminNotDeleted = $this->userRepository->findOneBy(['id' => $adminToDelete->getId()]);
        $this->assertNotNull($adminNotDeleted);
    }
}