<?php

namespace App\Tests\Entity;

use App\DataFixtures\TaskFixtures;
use App\DataFixtures\UserFixtures;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @codeCoverageIgnore
 */
class AbstractTestEntity extends KernelTestCase
{
    use FixturesTrait;

    protected $fixtures;
    protected $user;
    protected $entityManager;
    protected $validator;
    
    protected function setUp(): void
    {
        self::bootKernel();
        $this->fixtures = $this->loadFixtures([UserFixtures::class, TaskFixtures::class])->getReferenceRepository();
        $this->user = self::$container->get(UserRepository::class)->findOneByEmail($this->fixtures->getReference('account-1')->getEmail());
        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->validator = self::$kernel->getContainer()->get('validator');
    }

    /**
     * Personnalized Assertions
     *
     * @param integer $number
     * @return void
     */
    public function assertHasErrors($entity, int $number = 0)
    {
        $errors = self::$container->get('validator')->validate($entity);
        $messages = [];

        foreach($errors as $error){
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(', ', $messages));
    }
}