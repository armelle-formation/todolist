<?php

namespace App\Tests\Entity;

use DateTime;
use App\Entity\Task;
use App\Entity\User;
use App\Tests\Entity\AbstractTestEntity;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Doctrine\Common\Collections\ArrayCollection;

class TaskTest extends AbstractTestEntity
{
    use FixturesTrait;

    /**
     * Return a user Entity with data
     * @return Task
     */
    public function getEntity() : Task
    {
        $task = new Task();
        $task->setTitle("Tâche de test");
        $task->setContent("Description de la tâche de test qui fait plus de 20 caractères");
        $task->setCreatedAt(new \DateTime('2020-05-03 03:10:00'));
        $task->setAuthor($this->user);

        $this->entityManager->persist($task);

        return $task;
    }

    /**
     * Application of validation rules : valid entity
     * @return void
     */
    public function testValidEntity()
    {
        $task = $this->getEntity();
        $errors = $this->validator->validate($task);
        
        // Assert
        $this->assertEquals(0, count($errors));
        $this->assertInstanceOf(DateTime::class, $task->getCreatedAt());
        $this->assertInstanceOf(User::class, $task->getAuthor());
        $this->assertFalse($task->getIsDone());
    }

    /**
     * Application of validation rules : invalid entity
     * @return void
     */
    public function testInvalidEntity()
    {
        $task = $this->getEntity();
        $task->setTitle(null);
        $task->setContent("Description");

        $errors = $this->validator->validate($task);
        $this->assertEquals(2, count($errors));
    }

    public function testSaveTask()
    {
        $task = $this->getEntity();
        $this->entityManager->persist($task);

        $this->entityManager->flush();

        $this->assertNotNull($task->getId());
    }

    /**
     *
     */
    public function testDoneToggle()
    {
        $task = $this->getEntity();
        $this->assertFalse($task->getIsDone());

        $task->setIsDone(1);
        $this->assertTrue($task->getIsDone());
    }

    public function testToggle()
    {
        $task = $this->getEntity();
        $task->toggle(1);
        $this->assertTrue($task->getIsDone());
    }

    public function testTaskHasExpectedAuthor()
    {
        $task = $this->getEntity();
        $this->assertNotNull($task->getAuthor());

        $task->setAuthor($this->user);
        $this->assertSame($this->user, $task->getAuthor());
    }

    public function testIsAnonymous()
    {
        $task = new Task();
        $user = new User();
        $user->setUsername('anonymous');

        $task->setAuthor($user);
        $this->assertTrue($task->isAnonymous());

        $task->setAuthor($this->user);
        $this->assertFalse($task->isAnonymous());
    }
}
