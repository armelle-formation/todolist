<?php

namespace App\Tests\Entity;

use DateTime;
use App\Entity\Task;
use App\Entity\User;
use App\Tests\Entity\AbstractTestEntity;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\Email;

class UserTest extends AbstractTestEntity {

    use FixturesTrait;

    /**
     * Return a user Entity with data
     * @return User
     */
    public function getEntity() : User 
    {
        $user = new User();
        $user->setUsername('Jane Doe')
            ->setEmail('test@domain.fr')
            ->setPassword('Password#123')
            ->setRoles(['ROLE_USER']);

        $this->entityManager->persist($user);

        $task = new Task;
        $task->setTitle("Tâche de tests");
        $task->setContent("Tâche issue des tests unitaires");
        $task->setAuthor($user);

        $user->addTask($task);

        $this->entityManager->persist($task);

        return $user;
    }

    /**
     * Application of validation rules : valid entity
     * @return void
     */
    public function testValidEntity()
    {
        $user = $this->getEntity();

        // Asserts
        $this->assertInstanceOf(DateTime::class, $user->getCreatedAt());
        $this->assertInstanceOf(DateTime::class, $user->getUpdatedAt());
        $this->assertInstanceOf(ArrayCollection::class, $user->getTasks());

        $errors = $this->validator->validate($user);
        $this->assertEquals(0, count($errors));
    }

    /**
     * Application of validation rules : invalid entity
     * @return void
     */
    public function testInvalidEntity()
    {
        $user = $this->getEntity();
        $user->setUsername('i')
             ->setEmail('iiohoh')
             ->setCreatedAt(new \DateTime('2020-05-03 03:10:00'))
             ->setUpdatedAt(new \DateTime('2020-04-23 13:00:00'))
             ->setRoles(['ROLE_ANONYMOUS']);

        $errors = $this->validator->validate($user);
        $this->assertEquals(4, count($errors));
    }

    public function testSaveUser()
    {
        $user = $this->getEntity();
        $this->entityManager->flush();
        $this->assertNotNull($user->getId());
    }

    /**
     * 
     */
    public function testEmailUnicityValidation(){
        $user = $this->getEntity()->setEmail('armelle@todolist-oc.fr');
        $this->assertHasErrors($user, 1);
    }

    public function testUsernameUnicityValidation(){
        $user = $this->getEntity()->setUsername('Admin');
        $this->assertHasErrors($user,1);
    }

    public function testAddAndRemoveTask()
    {

        $user = $this->getEntity();

        $task = new Task();
        $task->setTitle('Tâche supplémentaire');
        $task->setContent('Ceci est une tâche de plus pour '. $user->getusername());
        $task->setAuthor($user);

        $user->addTask($task);
        
        $this->entityManager->persist($task);
        $this->entityManager->flush();

        $this->assertCount(2, $user->getTasks());

        $user->removeTask($task);
        $this->assertCount(1, $user->getTasks());
    }

    public function testIsAdmin()
    {
        $user = new User();
        $this->assertEquals(false, $user->IsAdmin());

        $user->setRoles(['ROLE_ADMIN']);
        $this->assertEquals(true, $user->IsAdmin());
    }

    public function testIsAnonymous()
    {
        $user = new User();
        $this->assertEquals(false, $user->IsAnonymous());

        $user->setUsername('anonymous');
        $this->assertEquals(true, $user->IsAnonymous());
    }
}