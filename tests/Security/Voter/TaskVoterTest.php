<?php

namespace App\Tests\Security\Voter;

use App\Entity\Task;
use App\Entity\User;
use App\Security\Voter\TaskVoter;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


class TaskVoterTest extends WebTestCase
{
    
    private $task;
    private $voter;
    private $token;
    private $user;

    public function setUp()
    {
        $this->security = $this->createMock(Security::class);
        $this->voter = new TaskVoter($this->security);
        $this->task = new Task();

        $this->user = $this->createMock(User::class);
        $this->user->method('getId')->willReturn(1);

        $this->token = new UsernamePasswordToken($this->user, 'credentials', 'memory');
       
    }

    public function testTaskVoterEditByAuthor()
    {
        $this->task->setAuthor($this->user);
        $this->assertSame(
            Voter::ACCESS_GRANTED, 
            $this->voter->vote($this->token, $this->task, ['EDIT'])
        );
    }

    public function testTaskVoterEditByOtherUser()
    {
        $taskUser = $this->createMock(User::class);
        $taskUser->method('getId')->willReturn(2);

        $this->task->setAuthor($taskUser);
        $this->assertSame(
            Voter::ACCESS_DENIED, 
            $this->voter->vote($this->token, $this->task, ['EDIT'])
        );
    }

    public function testTaskVoterEditByAnonymous()
    {
        $token = new AnonymousToken('secret', 'anonymous');
        $this->assertSame(
            Voter::ACCESS_DENIED, 
            $this->voter->vote($token, $this->task, ['EDIT'])
        );
    }

    public function testTaskVoterEditTaskAnonymousByAdmin()
    {
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(true);

        $this->voter = new TaskVoter($this->security);

        $taskUser = $this->createMock(User::class);
        $taskUser->method('isAnonymous')->willReturn(1);
        $this->task->setAuthor($taskUser);

        $this->assertSame(
            Voter::ACCESS_GRANTED, 
            $this->voter->vote($this->token, $this->task, ['EDIT'])
        );
    }

    public function testTaskVoterDeleteByAuthor()
    {
        $this->task->setAuthor($this->user);
        $this->assertSame(
            Voter::ACCESS_GRANTED, 
            $this->voter->vote($this->token, $this->task, ['DELETE'])
        );
    }

    public function testTaskVoterDeleteByAdmin()
    {
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(true);

        $this->voter = new TaskVoter($this->security);

        $taskUser = $this->createMock(User::class);
        $taskUser->method('isAnonymous')->willReturn(1);
        $this->task->setAuthor($taskUser);

        $this->assertSame(
            Voter::ACCESS_GRANTED, 
            $this->voter->vote($this->token, $this->task, ['DELETE'])
        );
    }

    public function testTaskVoterDeleteByOther()
    {
        $taskUser = $this->createMock(User::class);
        $taskUser->method('getId')->willReturn(2);
        $this->task->setAuthor($taskUser);

        $this->assertSame(
            Voter::ACCESS_DENIED, 
            $this->voter->vote($this->token, $this->task, ['DELETE'])
        );
    }
}