<?php

namespace App\Tests\Security\Voter;

use App\Entity\Task;
use App\Entity\User;
use App\Security\Voter\UserVoter;
use App\DataFixtures\UserFixtures;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class VoterTest extends WebTestCase
{
    private $token;
    private $user;

    public function setUp()
    {
        $this->security = $this->createMock(Security::class);
        $this->user = $this->createMock(User::class);
        $this->token = new UsernamePasswordToken($this->user, 'credentials', 'memory');
    }

    /**
     * Only userobject can edit a user
     */
    public function testUserVoterGetNoUser()
    {
        $token = new AnonymousToken('secret', 'anonymous');
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(false);

        $voter = new UserVoter($this->security);
        $this->assertSame(Voter::ACCESS_DENIED, $voter->vote($token, $this->user, ['EDIT']));
    }

     /**
     * Only Admin can create a user
     */
    public function testUserVoterCreateByAdmin()
    {
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(true);

        $voter = new UserVoter($this->security);

        $this->assertSame(
            Voter::ACCESS_GRANTED, 
            $voter->vote($this->token, $this->user, ['CREATE'])
        );
    }

     /**
     * Simple user connot access on create user page
     */
    public function testUserVoterCreateByUser()
    {
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(false);

        $voter = new UserVoter($this->security);

        $this->assertSame(
            Voter::ACCESS_DENIED, 
            $voter->vote($this->token, $this->user, ['CREATE'])
        );
    }

    /**
     * User can edit himself
     */
    public function testUserVoterEditByUser()
    {
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(false);

        $voter = new UserVoter($this->security);

        $this->assertSame(
            Voter::ACCESS_GRANTED, 
            $voter->vote($this->token, $this->user, ['EDIT'])
        );
    }

    /**
     * Only user can edit himself OR Admin can edit user
     */
    public function testUserVoterEditOtherUserByUser()
    {
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(false);
            $taskUser = $this->createMock(User::class);
        
        $this->user = $this->createMock(User::class);
        $this->user->method('getId')->willReturn(1);
        $token = new UsernamePasswordToken($this->user, 'credentials', 'memory');

        $userToEdit = $this->createMock(User::class);
        $userToEdit->method('getId')->willReturn(2);

        $voter = new UserVoter($this->security);
        $this->assertSame(Voter::ACCESS_DENIED, $voter->vote($token, $userToEdit, ['EDIT']));
    }

    /**
     * An admin cannot edit his own role
     */
    public function testUserVoterEditOwnRoleByAdmin()
    {
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(true);

        $voter = new UserVoter($this->security);

        $this->assertSame(
            Voter::ACCESS_DENIED, 
            $voter->vote($this->token, $this->user, ['ROLE_EDITION'])
        );
    }

    /**
     * An admin cannot edit his own role
     */
    public function testUserVoterEditUserRoleByAdmin()
    {
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(true);

        $voter = new UserVoter($this->security);
        $this->user->method('getId')->willReturn(1);
        $userToedit = $this->createMock(User::class);
        $userToedit->method('getId')->willReturn(2);

        $this->assertSame(
            Voter::ACCESS_GRANTED, 
            $voter->vote($this->token, $userToedit, ['ROLE_EDITION'])
        );
    }

    /**
     * An admin can delete an user
     */
    public function testUserVoterDeleteUserByAdmin()
    {
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(true);

        $voter = new UserVoter($this->security);
        $this->user->method('getId')->willReturn(1);
        $userToDel = $this->createMock(User::class);
        $userToDel->method('getId')->willReturn(2);

        $this->assertSame(
            Voter::ACCESS_GRANTED, 
            $voter->vote($this->token, $userToDel, ['DELETE'])
        );
    }

    /**
     * An user cannot delete himself
     */
    public function testUserVoterDeleteHimself()
    {
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(true);

        $voter = new UserVoter($this->security);
        $this->user->method('getId')->willReturn(1);
        $this->assertSame(
            Voter::ACCESS_DENIED, 
            $voter->vote($this->token, $this->user, ['DELETE'])
        );
    }

    /**
     * An user cannot delete an user
     */
    public function testUserVoterDeleteUserByUser()
    {
        $this->security
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(false);

        $voter = new UserVoter($this->security);
        $this->user->method('getId')->willReturn(1);
        $userToDel = $this->createMock(User::class);
        $userToDel->method('getId')->willReturn(2);

        $this->assertSame(
            Voter::ACCESS_DENIED, 
            $voter->vote($this->token, $userToDel, ['DELETE'])
        );
    }
}