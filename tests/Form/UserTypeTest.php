<?php

namespace App\Tests\Entity;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserTypeTest extends TypeTestCase
{
    private $authorizationChecker;
    private $hierarchyRoles;

    protected function setUp() : void
    {
        // mock any dependencies
        $this->authorizationChecker = $this->createMock(AuthorizationCheckerInterface::class);
        $this->hierarchyRoles = [
            "ROLE_USER" => [
                0 => "ROLE_USER"
            ],
            "ROLE_ADMIN" => [
                0 => "ROLE_USER",
                1 => "ROLE_ADMIN"
            ]
        ];

        parent::setUp();
    }

    // Check by mock the injected dependencies
    protected function getExtensions()
    {
        // create a type instance with the mocked dependencies
        $type = new UserType($this->hierarchyRoles, $this->authorizationChecker);

        // Test form extensions
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->validator
            ->method('validate')
            ->will($this->returnValue(new ConstraintViolationList()));
        $this->validator
            ->method('getMetadataFor')
            ->will($this->returnValue(new ClassMetadata(Form::class)));

        return array(
            new PreloadedExtension(array($type), array()),
            new ValidatorExtension($this->validator)
        );
    }

    public function testSubmitValidData()
    {
        
        $formData = array(
            'username' => 'Jane',
            'password' => array('first' => 'Password#123', 'second' => 'Password#123'),
            'email' => 'jane@domain.com',
            'roles' => ['ROLE_USER']
        );

        // will retrieve data from the form submission and check if FormType compiles
        $objectToCompare = new User();
        $objectToCompare->setRoles(['ROLE_USER']);
        $form = $this->factory->create(UserType::class, $objectToCompare);

        $object = new User();
        $object->setUsername($formData['username']);
        $object->setPassword($formData['password']['first']);
        $object->setEmail($formData['email']);
        $object->setRoles($formData['roles']);

        // submit the data to the form directly
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized()); // Check dataTransformer
        $this->assertEquals($object, $objectToCompare);
        $this->assertEquals($object->getUsername(), $form->get('username')->getData());
        $this->assertEquals($object->getPassword(), $form->get('password')->getData());
        $this->assertEquals($object->getEmail(), $form->get('email')->getData());
        //$this->assertEquals($object->getRoles(), $form->get('roles')->getData());

        // check the creation of the FormView
        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            if($key != 'roles'){
                $this->assertArrayHasKey($key, $children);
            }
        }
    }
}