# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][1] and this project adheres to [Semantic Versioning][2].

## [1.0.0][3] - 2020-05-20
### Fixed
- Task without users
- Task status change
- 404 response for assets files
- Deleting users
  
### Added
#### New functionalities
- Linking task to current user when creating a new task
- Only authors may edit their own tasks (Admin has all rights)
- Filtering of tasks (ToDo/Done, current user tasks, all tasks)
- Only admin are allowed to manage users
- Update older tasks without author automatically (link with a anonymous user)
- Specify a role for a user
  
#### Interactivity
- Translation of error messages.
- Recast of application's design and ergonomy.
- Adding a logo with a homepage link.
- Update of the [OpenClassRooms][9] logo.
- Improved website navigation (loginpage, homepage, navigation etc.)
- Username display on navbar
- Direct access to your own profile edition (admin only)
- Display of additional information for each task (date, author, etc.) on list tasks page.
- Customized error pages (403, 404, 500).
- Responsivity
- Sort tasks by creation date (most recent first)

#### Assets
- Rename `shop-homepage.css` CSS file name to `main.css`.
- Use of a single file image instead of two (same file - two differents cropping)
- Weight and size optimization of header image file
- Renamed header image file `web\img\todolist_content.jpg` to `public\assets\img\todolist.jpg`.
- Upgraded Bootstrap 3.3.7 to use new [Bootstrap 4.4][11] release.
- Using [FontAwesome 5.12][13] font to display icons.
- Inclusion of front libraries :
  - [Bootstrap][11]
  - [jQuery][12]
  - [FontAwesome][13]
  - [Isotope][10]
  
#### Maintenability
- Retrieving user roles in `Form\UserType` directly from the security configuration (Automatic Service Loading via `services.yaml`)
- Units and functional tests. See [code coverage reports][3] for more informations
- Continuous Integration (on develop branch pushing)
- Use of the [PHP-CS-Fixer][14] tool to fix code and follow standards.
- Add validation constraints to entities (with callbacks for update date validity)
- reorganization of views into partial files

#### Performance
- Class loading optimization
- Cache with invalidation for lists pages

#### Security
- Unicity constraints on User entiity (username and email).
- Adding an users role system
- Adding symfony guard for authentication
- Encryption of `DATABASE_URL` (as mentionned in [Symfony security best practices][15]).
- Use of Voters components for manage access and action autorizations
- Adding Https

#### Documentation
- Changelog file
- Documentation files ([UML diagrams][5], [Authentication][6], [ReadMe][7])
- [Contributing instructions][8]
- PHPDoc Annotations
  
### Changed
- Only authors may delete their own tasks
- Tasks with anonymous author may only be deleted by admin
- Update of Symfony framework version (3.1 t o 5.0.7) and all its dependencies
- Moving the favicon file in assets images folder
- Use of LifeCycle events in entities (instead of setting values in constructor)
- Using of a trait for createAt (to avoid DRY)
- Using of dependency injection instead of calling the container in controllers (as mentionned in [Symfony security best practices][16])
- Changing the maximum length of entities properties
- Update locale definition (en to fr)
- Simplification of the task toggle action
  
### Deprecated 
- Constraint of nullity true on the author of the task
  
### Removed
- SwiftMailer bundle
- Graphic files used anymore
- Glyphicons font
- HTML5Shiv.js and Respond.js librairies (more need to maintain compatibility with IE8)
- Done Tasks page (replace with a task filtering system)

  
## [0.0.1][0] - 2016-11-02
### Added
- MVP for project "Improve an existing project". Fork of this [repository][0]

[16]: https://symfony.com/doc/current/best_practices.html#use-dependency-injection-to-get-services
[15]: https://symfony.com/doc/current/best_practices.html#use-secret-for-sensitive-information
[14]: https://cs.symfony.com/
[13]: https://fontawesome.com
[12]: https://jquery.com
[11]: https://getbootstrap.com/docs/4.4/getting-started/introduction/
[10]: https://isotope.metafizzy.co
[9]: https://openclassrooms.com
[8]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/contributing/CONTRIBUTING.md
[7]: https://gitlab.com/armelle-formation/todolist/-/blob/master/README.md
[6]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/authentication/AUTHENTICATION.md
[5]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/diagrams/
[4]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/audit.pdf
[3]: http://gitlab.com/armelle-formation/todolist.git
[2]: https://keepachangelog.com/en/1.0.0/
[1]: https://semver.org/spec/v2.0.0.html
[0]: https://github.com/saro0h/projet8-TodoList