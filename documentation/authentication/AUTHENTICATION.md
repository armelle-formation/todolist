# Guide de l'authentification

## Symfony Security Component

Le composant Security de Symfony est un système complet et puissant qui permet de faciliter la gestion de la sécurité des applications web. 
Il faut toutefois veiller à bien différencier deux notions clés :
- Authentification :  qui êtes vous ? 
- Autorisation : avez vous accès à cette ressource ?

## Le besoin
Dans cette application :

1. Toutes les pages du site, à l'exception de la page de login, doivent être restreintes aux utilisateurs authentifiés
2. Les utilisateurs peuvent créer, éditer et supprimer leurs propres tâches
3. Les pages réservées à la gestion des utilisateurs (création, édition et suppression) ne sont accessibles qu'aux administrateurs.

Afin de mettre en place ces régles, deux rôles (`ROLE_ADMIN` et `ROLE_USER`) ont été mis en place pour les utilisateurs.

## La mise en place de l'authentification

### La Classe User
L'entité App\Entity\User est l'entité qui décrit l'utilisateur de l'application. Deux contraintes d'unicité sont spécifiées : sur la propriété `username` et sur la propriété `email` afin de ne pas avoir de doublons.

```php
/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *  fields={"email"},
 *  message="Cet email a déjà été utilisé, merci de le modifier"
 * )
 * @UniqueEntity(
 *  fields={"username"},
 *  message="Ce nom d'utilisateur existe déjà dans notre liste d'utilisateurs."
 * )
 */
class User implements UserInterface
{
```
La classe User hérite de `UserInterface`, une interface créée par Symfony qui facilite l'accès à quelques fonctions de bases.

### Le fichier de configuration security.yml
La sécurité, et en particulier l'authentification, est entièrement configurée dans le fichier `config/packages/security.yml`. Ce fichier comporte plusieurs sections :

- `encoders` : détermine le type d'encodage pour le mot de passe
- `provider` : le fournisseur de données utilisateurs
- `firewalls` : la partie la plus importante : le coeur du sytème d'authentification.
- `role_hierarchy` : le système de rôles
- `access_control` : la restriction des routes

### L'encoder
L'encoder contient les informations sur la clé de cryptage utilisée pour les mots de passe. Il définit l'algorythme et le namespace de l'entité utilisée pour l'authentification.
```yaml
# config/packages/security.yml
security:
    encoders:
        App\Entity\User:
            algorithm: bcrypt
```

### Le provider
Le provider est le composant qui va permettre de récupérer les informations de l'utilisateur depuis une source de données. 
Les utilisateurs étant enregistrés en base de données, ce provider a été nommé `in_database` (cela peut changer si le moyen d'accéder aux données change).
Ce provider utilisera la classe `User` que nous avons définie. L'attribut `username` servira d'identifiant à l'utilisateur pour se connecter.

```yaml
# config/packages/security.yml
providers:
    in_database: 
        entity:
            class: App\Entity\User
            property: username
```

### Le firewall
Les firewalls (pare-feu) vont définir le système d'authentification de l'application. C'est en quelque sorte le bureau de sécurité de l'application. Il va permettre d'interdire à un utilisateur non authentifié d’accéder à certaines parties du site.

Deux firewalls sont definis pour notre application : `dev` et `main` mais il n'y a toujours qu'un seul pare-feu actif sur chaque demande. Si vous accédez à une URL qui commence par / _profiler, / _wdt ou / css, vous interrogez uniquement le pare-feu de développement : c'est comme se faufiler par la porte latérale d'un bâtiment. 
Le firewall `dev` sert juste à nous assurer qu'aucun outil de débogage ne serait sécurisé accidentellement.

Le firewall `main` (principal) définit réellement l'accès a notre application. Toute requête entrante passe par ce pare-feu. Celui-ci contient une clé `anonymous` qui vaut `true`, c'est ce qui nous permet d'accéder au site sans être authentifié (c'est cette clé que nous pouvons retrouver dans le profiler Symfony en environnement de developpement) et d'utiliser le role sytème `IS_AUTHENTICATED_ANONYMOUSLY`.

**Attention** : L'ordre des pare-feux est important ! C'est le premier correspondant à l'itinéraire demandé qui est interrogé.

```yaml
# config/packages/security.yml
firewalls:
    dev:
        pattern: ^/(_(profiler|wdt)|css|images|js)/
        security: false
    main:
        anonymous: true
        provider: in_database
```
### L'authentification par formulaire

 `form_login` indique au pare feu que les utilisateurs sont authentifiés via un formulaire. Ce formulaire est accessible via la route `login`, comme définit par `login_path`. 
 
 `check_path` est le nom de la route qui conduira à la méthode de vérification des informations du formulaire (ici, la même que celle de l'affichage du formulaire).
```yaml
firewalls:
    main:
        ...
        form_login:
            login_path: login
            check_path: login
                
            logout:
                path: logout
                target: login

```
Le formulaire de login n'est pas un formulaire construit avec le sytème de `formType` de Symfony, aussi la faille CSRF n'est pas géré automatiquement. C'est pourquoi le champ `_csrf_token` a été ajouté manuellement au formulaire.
```html
<form action="{{ path('login') }}" method="post">
    <input type="hidden" name="_csrf_token" value="{{ csrf_token('authenticate') }}">
```

### Guard
Le `Guard` est un composant Symfony qui permet de personnaliser l'authentification mais aussi de rendre évolutif le formulaire de connexion en implémentatnt une stratégie d'authentification. Il doit être spécifié dans le fichier `security.yml`
```yaml
# config/packages/security.yml
firewalls:
    ...
    guard:
        authenticators:
            - App\Security\LoginFormAuthenticator
```
Dans cette application, le `Guard` permet surtout de :
* vérifier le jeton csrf défini dans le formulaire de login personnalisé.
* personnaliser les messages d'erreurs
* de définir la redirection de manière précisé en fonction du resultat de l'authentification

#### Redirection
La méthode `onAuthenticationSuccess` du `Guard` permet de configurer la destination de la redirection lorsque l'authentification se déroule correctement.
```php
public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->urlGenerator->generate('homepage'));
    }
```
#### Messages d'erreurs
Grâce au Guard, il est possible de personnaliser les messages d'erreurs en fonctions des différents cas de figures (username non trouvé, identifiants invalides....)
```php
// src/Security/LoginFormAuthenticator.php
public function getUser($credentials, UserProviderInterface $userProvider)
{
    $token = new CsrfToken('authenticate', $credentials['csrf_token']);
    if (!$this->csrfTokenManager->isTokenValid($token)) {
        throw new InvalidCsrfTokenException();
    }

    $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);
    
    // fail authentication with a custom error
    if (!$user) {
        throw new CustomUserMessageAuthenticationException('Utilisateur inconnu');
    }

    return $user;
}
```

Les messages d'erreur ainsi retournés par le Guard sont récupérés par le `SecurityController` et passés à la vue

```php
// src/Controller/SecurityController.php
/**
* @Route("/login", name="login")
* @return Response
*/
public function login(AuthenticationUtils $utils)
{
    $error = $utils->getLastAuthenticationError();
    $username = $utils->getLastUsername();

    return $this->render(
        'security/login.html.twig',
        array(
            'username' => $username,
            'error'    => $error,
        )
    );
}
```

## La mise en place des Autorisations
### Role Hierarchy

La hirérarchie des utilisateurs permet à un rôle d'hériter des prérogatives d'autres rôles. Ainsi, avec notre configuration, un utilisateur `ROLE_ADMIN` aura toutes les prérogatives d'un utilisateur `ROLE_USER`.

Utiliser la hiérarchie dans notre application :
* facilite la structure de la base de données (pas besoin de relation Many-To-Many : seul le rôle le plus haut est enregistré en base pour chaque utilisateur)
* permet à l'application d'être évolutive. Pour l'instant notre application est assez simple mais, elle pourrait à terme disposer de plusieurs rôles.
```yaml
# config/packages/security.yml
security:
    role_hierarchy:
        ROLE_USER: ROLE_USER
        ROLE_ADMIN: [ROLE_USER, ROLE_ADMIN]
```

### Access Control
Seule la homepage de l'application est accessible aux utilisateurs non connectés. Pour toutes les autres, l'utilisateur doit être authentifié.
Celles dont la route commence par « /users » sont réservées aux utilisateurs ayant le rôle `ROLE_ADMIN`. Les chemins sont fournis sous la forme d'expression régulière.

```yaml
# config/packages/security.yml
    access_control:
        - { path: ^/login$, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/users, roles: ROLE_ADMIN }
        - { path: ^/, roles: ROLE_USER }
```
**A noter** : la propriété `access_control` redirigera l'utilisateur conformément au paramétrage du `guard`. Si aucune redirection n'est renseignée, la page d'erreur 403 s'affichera.
Symfony s'arrêtera sur la première occurence d'accès qu'il trouvera dans la liste, c'est pourquoi, la liste des pages publiques (accessibles sans authentification) doit se trouver en premier.

### Autorisation 
#### Granted
L'`access_control` est utilisé pour verouiller les grandes sections de l'application mais ce système d’autorisation est renforcé par l'emploi des annotations isGranted à l'intérieur des contrôleurs :

```php
// src/Controller/UserController.php
/**
 * Class UserController
 * @IsGranted("ROLE_ADMIN")
 * @Route("/users", name="user_")
 */
class UserController extends AbstractController
{
```
Cette annotation est ici redondante puisque la section entière est protégée par l'`accesss_control` mais on peut imaginer qu'une prochaine évolution de l'application serait d'autoriser les utilisateurs à éditer leurs profils : l'utilisation de l'annotation sera ici beaucoup plus flexible puisqu'en l'association simplement aux méthodes voulues, la nouvelle autorisation pourra être mise en place (couplée à la modification de l'`accesss_control`)

#### Voters
Le gestion des autorisations est également affinée grâce aux Voters. Les voters offre une meilleure granularité pour vérifier une autorisation (cet utilisateur a t-il le droit d'effectuer cette action sur cet élément donné ?). Ils permettent également de centraliser une logique d'autorisation.

On demande ici à l'UserVoter de vérifier l'autorisation pour l'action `DELETE` sur l'utilisateur appelé pour l'utilisateur connecté.

```php
// src/Controller/TaskController.php
/**
     * @Route("/{id}/delete", name="delete")
     * @IsGranted("DELETE", subject="user")
     *
     * @param User $user
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function deleteAction(User $user, EntityManagerInterface $manager)
    {
```
Ainsi, bien que la suppression d'un utilisateur ne soit possible que par un administrateur (avec l'`access_control`), on précise qu'on ne souhaite pas qu'un utilisateur puisse se supprimer lui-même ou éditer son propre rôle, tout administrateur qu'il soit.

```php
// src/Security/Voter/UserVoter.php
switch ($attribute) {
    case 'ROLE_EDITION':
    case 'DELETE':
        if ($user != $connectedUser && $this->authorizedUserIsAdmin()) {
            return true;
        }
    break;
}
```

Les voters permettent également d'effectuer les vérifications directement dans les vues :
```html
     {% if is_granted('EDIT', task) %}
        <form action="{{ path('task_edit', { 'id' : task.id }) }}">
            <button class="btn btn-info btn-sm pull-right btn-edit">
                <i class="fas fa-edit"></i> Modifier
            </button>
        </form>
    {% endif %}
    {% if is_granted('DELETE', task) %}
        <form action="{{ path('task_delete', { 'id' : task.id }) }}">
            <button class="btn btn-danger btn-sm pull-right btn-delete">
                <i class="fas fa-trash-alt"></i> Supprimer
            </button>
        </form>
    {% endif %}
```
### Conclusion
Avec toutes ces informations, vous êtes maintenant en mesure de gérer la sécurité de cette application, d'ajouter un autre rôle, de restreindre à un rôle spécifique un contrôleur ou un chemin d'accès, de modifier les autorisations utilisateur...

Toutefois, n'oubliez de consulter la [documentation du Security Component de Symfony][1].

Vous pouvez également d'autres informations utiles dans les fichiers [Readme][2], [ChangeLog][4] et [Contributing][3] de ce repository.


[1]: https://symfony.com/doc/current/components/security.html
[2]: https://gitlab.com/armelle-formation/todolist/-/blob/master/README.md
[3]: https://gitlab.com/armelle-formation/todolist/-/blob/master/CONTRIBUTING.md
[4]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/releases_notes/CHANGELOG.md