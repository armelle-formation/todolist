# How to contribute

TodoList App is a collaborative project. 
Please, read the [ReadMe][6] and [ChangeLog][20] files first.
Follow those steps for contributing to this project.


## Report a bug or propose a feature
You can report a bug or request the addition of a feature on the application by creating the issue on the project repository.

Please respect these few rules:
* Give a title as clear and short as possible
* Clearly describe the problem. In the case of a bug, give as much detail as possible about your environment (OS, PHP version, extensions ...) and describe the steps to reproduce the bug
* Use the bug or functionality label for your issue

## Instructions
### A. Fork this repository
A *fork* is a copy of an original repository that you put in another namespace where you can experiment and apply changes without affecting the original project.. Forking a repository lets you to make changes to your copy without affecting any of the original code.

1. On the project’s home page, in the top right, click the Fork button.
2. Click a namespace to fork to. Only namespaces you have Developer and higher permissions for are shown.

### B. Clone your fork
A *clone* is a downloaded version of a repository. Cloning our fork lets you download a copy of the repository to your computer.

Clone the fork on your locale machine by running this command line: `git clone {your fork uri}`.

**Be careful!** If you created your fork a while ago be sure to pull upstream changes into your local repository with this command: `git pull upstream master && git push origin master`.


### C. Install project
Install the project following [README.md][6]

### D. Work on project

1. Create a new branch
Create your feature branch
`git checkout -b [prefix]/[name]`

Use our conventional prefixes according to your contribution:

- `hotfix/` : for updates, fixes
- `feature/` : for new features

2. Code your new feature or bug fix
  
3. Implement your own tests and test your code
  This project has unit and functional tests so run them regularly during your dev to check everything is still fine. 
  **Attention** to not decrease code coverage! You can find code coverage in [coverage][13] folder.

    Useful documentation:
    - [PHPUnit documentation][14]
    - [Symfony Testing][15]

4. Verify your code quality
Before pushing your code, it is advisable to analyze it with a tool like [SonarQube][19].

5. Commit your code
`git commit -am 'add some feature'`

5. Push to the branch
`git push origin my-new-feature`

### E. Create a merge request

Go to your fork on your Gitlab account to open a merge request. Provide clear title and concise explanations about your work in your merge request description. Add issue reference.

Now you're sure about your work you can click on "Create merge request" and target project `develop` branch.

Gitlab CI will automatically run tests and code will be checked with php-cs-fixer. Your code must succeed on syntax and testing stage to have a chance to be merged.

If your pull request complies with the project contribution recommendations, your feature or correction will be integrated into the application.

## Coding conventions 

#### Standards
Your code must respects :
- [PHP Standards Recommendations (PSR)][7]
  - [PSR-1][8]
  - [PSR-12][9]
  - [PSR-4][10]
- [Symfony Coding Standards][11]

### Best Practices
- [Symfony Best Practices][12]
- [Doctrine Best Practices][21]

### Naming convention
* Variables, functions and arguments must be named in camelCase
* Use namespaces for all your classes and name them in UpperCamelCase
* Use the suffix Interface for interfaces
* Use the line suffix for lines
* Use the Exception suffix for exceptions
* Avoid DRY (don't repeat yourself)

### Documentation
* PHPDoc must be present for all classes and functions
* No @return tag if the method returns nothing
* Use multiple lines if necessary for PHPDoc annotations
* Add the @author tag with your information on major changes such as adding a new class

## Useful resources

- A [technical documentation][16] about *authentication* in To Do List App
- [GitLab Documentation][17] - GitHub full documentation
- [GitLab Git CheatSheet][18] - PDF


[1]: https://www.mysql.com/fr/
[2]: http://php.net/manual/fr/intro-whatis.php
[3]: https://www.apache.org/
[4]: https://getcomposer.org/download/
[5]: https://phpunit.de
[6]: https://gitlab.com/armelle-formation/todolist/-/blob/master/README.md
[7]: https://www.php-fig.org/psr
[8]: https://www.php-fig.org/psr/psr-1
[9]: https://www.php-fig.org/psr/psr-12/
[10]: https://www.php-fig.org/psr/psr-4
[11]: https://symfony.com/doc/current/contributing/code/standards.html
[12]: https://symfony.com/doc/current/best_practices.html
[13]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/codecoverage/v1.0/report.html
[14]: https://phpunit.de/documentation.html
[15]: https://symfony.com/doc/current/testing.html
[16]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/authentication/AUTHENTICATION.md
[17]: https://docs.gitlab.com/
[18]: https://about.gitlab.com/images/press/git-cheat-sheet.pdf
[19]: https://www.sonarqube.org/
[20]: https://gitlab.com/armelle-formation/todolist/-/blob/master/documentation/releases_notes/CHANGELOG.md
[21]: https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/reference/best-practices.html
